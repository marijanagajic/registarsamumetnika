<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book"></i> Репрезентативна удружења
			</h1>

			<table class="table table-hover">
				<thead class="thead-dark">
					<tr>
						<th class='text-center'>Назив</th>
						<th class='text-center'>Седиште</th>
						<th class='text-center'>Матични број</th>
						<th class='text-center'>ПИБ</th>
						<th class='text-center'>Статус</th>
						<th class='text-center'>Датум укидања статуса</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="trenutnoUdruzenje" items="${udruzenja}">
						<c:url var="izmeniLink"
							value="/administracija/udruzenja/izmeni-udruzenje">
							<c:param name="udruzenjeId"
								value="${trenutnoUdruzenje.udruzenjeId}" />
						</c:url>
						<c:url var="obrisiLink"
							value="/administracija/udruzenja/obrisi-udruzenje">
							<c:param name="udruzenjeID"
								value="${trenutnoUdruzenje.udruzenjeId}" />
						</c:url>
						<c:url var="udruzenjeLink"
							value="/administracija/udruzenja/${trenutnoUdruzenje.pib}">
						</c:url>

						<tr onclick="window.location='${udruzenjeLink}';">
							<td><strong>${trenutnoUdruzenje.naziv}</strong></td>
							<td>${trenutnoUdruzenje.sediste}</td>
							<td>${trenutnoUdruzenje.maticniBroj}</td>
							<td>${trenutnoUdruzenje.pib}</td>
							<td>${(trenutnoUdruzenje.status==1)? "Важећи":"Укинут"}</td>
							<td>${trenutnoUdruzenje.datumukidanjastatusa}</td>
						<tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

	</div>


	<%@ include file="footer.jsp"%>


</body>

</html>
