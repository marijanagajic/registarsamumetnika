<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book"></i> Уметници
			</h1>

			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th class='text-center'>Име</th>
						<th class='text-center'>Презиме</th>
						<th class='text-center'>Врста</th>
						<th class='text-center'>Удружење</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="trenutniUmetnik" items="${umetnici}">
						<c:url var="izmeniLink"
							value="/administracija/umetnici/izmeni-podatke">
							<c:param name="jmbg" value="${trenutniUmetnik.jmbg}" />
						</c:url>

						<c:url var="umetnikLink"
							value="/administracija/umetnici/${trenutniUmetnik.jmbg}">
						</c:url>

						<sec:authorize
							access="hasAuthority('Superadmin') OR (hasAuthority('Udruzenje') AND ${(trenutniUmetnik.proglasenOdUdruzenja.udruzenjeId == korisnik.udruzenje.udruzenjeId)})">
							<tr onclick="window.location='${umetnikLink}';">
						</sec:authorize>
						<sec:authorize
							access="(!hasAuthority('Superadmin') AND !hasAuthority('Udruzenje')) OR (hasAuthority('Udruzenje') AND ${(trenutniUmetnik.proglasenOdUdruzenja.udruzenjeId != korisnik.udruzenje.udruzenjeId)})">
							<tr>
						</sec:authorize>
						<td>${trenutniUmetnik.ime}</td>
						<td>${trenutniUmetnik.prezime}</td>
						<td>${trenutniUmetnik.tipUmetnika.nazivTipa}</td>
						<td>${trenutniUmetnik.proglasenOdUdruzenja.naziv}</td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

		<sec:authorize
			access="hasAuthority('Superadmin') OR hasAuthority('Udruzenje')">
			<div class="margintop10">
				<input type="button" value="Додај новог уметника"
					class="form-control btn-info text-center"
					onclick="window.location.href='${pageContext.request.contextPath}/administracija/umetnici/dodaj-novog-umetnika'; return false;" />
			</div>
		</sec:authorize>
		<sec:authorize
			access="hasAuthority('Superadmin') OR hasAuthority('Ministarstvo')">
			<div class="margintop10">
				<input type="button" value="Сви уметници (активни и неактивни)"
					class="form-control btn-info text-center"
					onclick="window.location.href='${pageContext.request.contextPath}/administracija/svi-umetnici'; return false;" />
			</div>
		</sec:authorize>
	</div>

	<%@ include file="footer.jsp"%>


</body>

</html>
