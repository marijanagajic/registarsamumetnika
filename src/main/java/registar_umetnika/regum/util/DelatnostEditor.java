package registar_umetnika.regum.util;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.TipUmetnika;
import registar_umetnika.regum.entity.Udruzenje;
import registar_umetnika.regum.service.interfaces.DelatnostService;
import registar_umetnika.regum.service.interfaces.MestoService;
import registar_umetnika.regum.service.interfaces.UdruzenjeService;
import registar_umetnika.regum.service.interfaces.UmetnikService;

@Component
public class DelatnostEditor extends PropertyEditorSupport {
	
	@Autowired
	private DelatnostService delatnostService;
	
	public void setAsText(String id) {
		Delatnost d = this.delatnostService.vratiDelatnostById(Integer.valueOf(id));
		this.setValue(d);
	}
}
