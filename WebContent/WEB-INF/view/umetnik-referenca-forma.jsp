<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad">
		<p class="demo naslov">
			<b>Унос нове референце</b>
		</p>
		<p style="text-align: center">Молимо Вас попуните податке</p>

		<div class="container">
			<form:form action="sacuvaj-novu-referencu" modelAttribute="referenca"
				method="POST">
				<form:hidden path="referencaId" />

				<div class="row">
					<div class="col-25">
						<label for="Naziv">Назив</label>
					</div>
					<div class="col-75">
						<form:input required="required" type="text" path="naziv" cssClass="form-control"
						 />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label for="datum">Датум одржавања</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="datum" type="date" cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label for="opis">Опис</label>
					</div>
					<div class="col-75">
						<form:textarea required="required" path="opis"
							style="height: 200px"></form:textarea>
					</div>
				</div>

				<div class="row">
					<label class="margintop10" for="submit"></label> <input
						type="submit" name="pocni" value="Potvrdi"
						class="form-control btn-primary btn-xl">
				</div>
				<div class="row">
					<input type="button" value="Odustani"
						class="form-control btn-info text-center"
						onclick="window.location.href='${pageContext.request.contextPath}/administracija/umetnici'; return false;" />
				</div>
			</form:form>
		</div>
	</div>

	<%@ include file="footer.jsp"%>

</body>

</html>
