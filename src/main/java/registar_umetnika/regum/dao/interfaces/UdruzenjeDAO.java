package registar_umetnika.regum.dao.interfaces;

import java.util.List;

import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Udruzenje;

public interface UdruzenjeDAO {

	List<Udruzenje> vratiUdruzenja();
	
	List<Udruzenje> vratiAktivnaUdruzenja();
	
	List<Udruzenje> vratiAktivnaUdruzenjaIdNaziv();

	void sacuvajUdruzenje(Udruzenje novoUdruzenje);

	KulturnoPodrucje vratiPodrucjePoIDu(Long valueOf);

	void obrisiUdruzenje(int id);

	Udruzenje vratiUdruzenje(int id);
	
	Udruzenje vratiUdruzenjeIdNaziv(int id);

	Udruzenje vratiUdruzenje(String text);

	List<Udruzenje> vratiUdruzenja(int podrucjeId);

	Udruzenje vratiUdruzenje(String property, String value);

}
