package registar_umetnika.regum.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.Korisnik;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.Organ;
import registar_umetnika.regum.entity.Udruzenje;
import registar_umetnika.regum.entity.Uloga;
import registar_umetnika.regum.entity.Umetnik;
import registar_umetnika.regum.service.interfaces.KorisnikService;
import registar_umetnika.regum.service.interfaces.OrganService;
import registar_umetnika.regum.service.interfaces.UdruzenjeService;
import registar_umetnika.regum.service.interfaces.UmetnikService;
import registar_umetnika.regum.util.KorisnikEditor;
import registar_umetnika.regum.util.OrganEditor;
import registar_umetnika.regum.util.UdruzenjeEditor;
import registar_umetnika.regum.util.UlogaEditor;

@Controller
@RequestMapping("/administracija")
@SessionAttributes({ "umetnik" })
public class KorisnikController {

	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private UmetnikService umetnikService;
	@Autowired
	private UdruzenjeService udruzenjeService;
	@Autowired
	private OrganService organService;
	@Autowired
	private KorisnikEditor korisnikEditor;
	@Autowired
	private UlogaEditor ulogaEditor;
	@Autowired
	private OrganEditor organEditor;
	@Autowired
	private UdruzenjeEditor udruzenjeEditor;

	@RequestMapping("/korisnici")
	public String prikaziKorisnike(Model theModel) {
		List<Korisnik> korisnici = korisnikService.vratiAktivneKorisnike();
		theModel.addAttribute("korisnici", korisnici);

		return "lista-korisnika";
	}
	
	@RequestMapping("/korisnici-svi")
	public String prikaziKorisnikeSvi(Model theModel) {
		List<Korisnik> korisnici = korisnikService.vratiKorisnike();
		theModel.addAttribute("korisnici", korisnici);

		return "lista-korisnika-svi";
	}

	@GetMapping("/korisnici/dodaj-novog-korisnika")
	public String dodajKorisnika(Model theModel, @RequestParam(value = "umetnikID", required = false) String id,
			@ModelAttribute("umetnik") Umetnik umetnik) {

		Korisnik k = new Korisnik();
		List<Uloga> uloge = korisnikService.vratiUloge();
		umetnik = umetnikService.vratiUmetnika("umetnikid", id);
		if (id != null) {
			theModel.addAttribute("umetnik", umetnik);

			k.setIme(umetnik.getIme());
			k.setPrezime(umetnik.getPrezime());
			k.setEnabled(true);

			for (Uloga u : uloge) {
				if (u.getNazivUloge().equals("Umetnik")) {
					k.setUloga(u);
					break;
				}
			}

			theModel.addAttribute("uloge", uloge);
			theModel.addAttribute("korisnik", k);

			return "korisnik-forma-za-umetnika";
		}

		theModel.addAttribute("uloge", uloge);
		theModel.addAttribute("korisnik", k);

		return "korisnik-forma";
	}

	@PostMapping("/korisnici/sacuvaj-korisnika")
	public String sacuvajKorisnika(@ModelAttribute("korisnik") Korisnik korisnik,
			@ModelAttribute("umetnik") Umetnik umetnik, RedirectAttributes redir) {
	
		if ((korisnik.getKorisnikId() != null && 
				!korisnikService.vratiKorisnika("korisnikId", String.valueOf(korisnik.getKorisnikId())).getUsername().equals(korisnik.getUsername()))
				|| (korisnik.getKorisnikId() == null)
				&& korisnikService.vratiKorisnika("username", korisnik.getUsername()) != null) {
			redir.addFlashAttribute("notification", "Корисничко име већ постоји! Изаберите друго корисничко име.");
			if (umetnik != null) {
				return "redirect:/administracija/korisnici/dodaj-novog-korisnika?umetnikID=" + umetnik.getUmetnikId();
			} else {
				return "redirect:/administracija/korisnici/izmeni-korisnika?korisnikId="+korisnik.getKorisnikId();
			}
		}
		korisnikService.sacuvajKorisnika(korisnik);

		if (umetnik != null) {
			umetnik.setKorisnik(korisnik);
			umetnikService.sacuvajUmetnika(umetnik);
			return "redirect:/administracija/umetnici/" + umetnik.getJmbg();
		}

		return "redirect:/administracija/korisnici";
	}

	@GetMapping("/korisnici/izmeni-korisnika")
	public String izmeniKorisnika(@RequestParam("korisnikId") int id, Model theModel) {
		Korisnik k = korisnikService.vratiKorisnika("korisnikid", id + "");

		List<Uloga> u = korisnikService.vratiUloge();

		theModel.addAttribute("korisnik", k);
		theModel.addAttribute("uloge", u);


		return "korisnik-forma";
	}

	@RequestMapping("/korisnici/uloge")
	public String prikaziUloge(Model theModel) {

		List<Uloga> uloge = korisnikService.vratiUloge();
		theModel.addAttribute("uloge", uloge);

		return "lista-uloga";
	}

	@GetMapping("/korisnici-po-ulozi")
	public String prikaziKorisnikePoUlozi(@RequestParam(name = "ulogaId") int idUlogeZaPrikaz, Model theModel) {
		List<Korisnik> korisnici = korisnikService.vratiKorisnike(idUlogeZaPrikaz);

		theModel.addAttribute("korisnici", korisnici);

		return "lista-korisnika";
	}

	@GetMapping("/korisnici/deaktiviraj-korisnika")
	public String obrisiKorisnika(@RequestParam("korisnikId") String id) {
		Korisnik k = korisnikService.vratiKorisnika("korisnikId", id);
		k.setEnabled(false);
		korisnikService.sacuvajKorisnika(k);

		return "redirect:/administracija/korisnici";
	}

	@ModelAttribute("umetnik")
	private Umetnik umetnik() {
		return null;
	}
	
	@ModelAttribute("organi")
	public List<Organ> organi() {
		return organService.vratiOrganeIdNaziv();
	}
	
	@ModelAttribute("udruzenja")
	public List<Udruzenje> udruzenja() {
		return udruzenjeService.vratiAktivnaUdruzenjaIdNaziv();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Uloga.class, this.korisnikEditor);
		binder.registerCustomEditor(Uloga.class, this.ulogaEditor);
		binder.registerCustomEditor(Udruzenje.class, this.udruzenjeEditor);
		binder.registerCustomEditor(Organ.class, this.organEditor);
	}
}
