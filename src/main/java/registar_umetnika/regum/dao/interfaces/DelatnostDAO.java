package registar_umetnika.regum.dao.interfaces;

import java.util.List;

import registar_umetnika.regum.entity.Delatnost;

public interface DelatnostDAO {

	List<Delatnost> vratiDelatnosti();
	Delatnost vratiDelatnostById(int id);
	

}
