package registar_umetnika.regum.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import registar_umetnika.regum.dao.interfaces.DelatnostDAO;
import registar_umetnika.regum.dao.interfaces.KulturnoPodrucjeDAO;
import registar_umetnika.regum.dao.interfaces.MestoDAO;
import registar_umetnika.regum.dao.interfaces.OrganDAO;
import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.Organ;

@Repository
public class OrganDAOImpl implements OrganDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public Organ vratiOrgan(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query theQuery = currentSession.createQuery("from Organ o where o.organId="+id, Organ.class);
		return (Organ)theQuery.getSingleResult();
	}
	
	@Override
	public List<Organ> vratiOrgane() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query theQuery = currentSession.createQuery("from Organ", Organ.class);
		return theQuery.getResultList();
	}

	@Override
	public List<Organ> vratiOrganeIdNaziv() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query theQuery = currentSession.createQuery("select new Organ(o.organId, o.naziv) from Organ o", Organ.class);
		return theQuery.getResultList();
	}
	
	
		
}
