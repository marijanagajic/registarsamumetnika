package registar_umetnika.regum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import registar_umetnika.regum.dao.interfaces.DelatnostDAO;
import registar_umetnika.regum.dao.interfaces.KulturnoPodrucjeDAO;
import registar_umetnika.regum.dao.interfaces.MestoDAO;
import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.service.interfaces.DelatnostService;
import registar_umetnika.regum.service.interfaces.KulturnoPodrucjeService;
import registar_umetnika.regum.service.interfaces.MestoService;

@Service
public class DelatnostServiceImpl implements DelatnostService {
	
	@Autowired
	private DelatnostDAO delatnostDAO;
	
	@Override
	@Transactional
	public List<Delatnost> vratiDelatnosti() {
		return delatnostDAO.vratiDelatnosti();
	}

	@Override
	@Transactional
	public Delatnost vratiDelatnostById(int id) {
		return delatnostDAO.vratiDelatnostById(id);
	}

	
}
