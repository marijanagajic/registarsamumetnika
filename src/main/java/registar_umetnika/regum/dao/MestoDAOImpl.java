package registar_umetnika.regum.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import registar_umetnika.regum.dao.interfaces.KulturnoPodrucjeDAO;
import registar_umetnika.regum.dao.interfaces.MestoDAO;
import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;

@Repository
public class MestoDAOImpl implements MestoDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Mesto> vratiMesta() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query theQuery = currentSession.createQuery("from Mesto", Mesto.class);
		return theQuery.getResultList();
	}

	@Override
	public Mesto vratiMestoById(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query theQuery = currentSession.createQuery("from Mesto m where m.mestoId="+id, Mesto.class);
		return (Mesto)theQuery.getSingleResult();
	}
	
		
}
