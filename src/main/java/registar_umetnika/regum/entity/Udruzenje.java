package registar_umetnika.regum.entity;

import java.io.Serializable;
import java.sql.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;
import org.omg.PortableServer.IdUniquenessPolicy;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Entity
@Table(name = "udruzenje")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Udruzenje implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "udruzenjeId")
	private int udruzenjeId;

	@Column(name = "maticnibroj")
	private int maticniBroj;

	@Column(name = "pib")
	private int pib;

	@Column(name = "naziv")
	private String naziv;

	@Column(name = "sediste")
	private String sediste;

	@Column(name = "adresa")
	private String adresa;

	@Column(name = "datumosnivanja")
	private Date datumOsnivanja;

	@Column(name = "zastupnik")
	private String zastupnik;
	
	@Column(name = "registarskibroj")
	private String registarskiBroj;
	
	@Column(name = "url")
	private String url;
	
	@Column(name = "status")
	private int status;
	
	@Column(name="datumukidanjastatusa")
	private Date datumukidanjastatusa;

	@OneToMany(mappedBy = "udruzenje", cascade = CascadeType.ALL)
	private Set<PripadnostUdruzenja> pripadnostiUdruzenja = new HashSet<>();

	public Udruzenje(int udruzenjeId) {
		super();
		this.udruzenjeId = udruzenjeId;
	}

	public Udruzenje(int udruzenjeId, int maticniBroj, int pib, String naziv, String sediste,
			Set<PripadnostUdruzenja> pripadnostiUdruzenja) {
		super();
		this.udruzenjeId = udruzenjeId;
		this.maticniBroj = maticniBroj;
		this.pib = pib;
		this.naziv = naziv;
		this.sediste = sediste;
		this.pripadnostiUdruzenja = pripadnostiUdruzenja;
	}

	public Udruzenje(int udruzenjeId, String naziv) {
		super();
		this.udruzenjeId = udruzenjeId;
		this.naziv = naziv;
	}

	 @Override
	    public String toString() {
	        return "Udruzenje id=" + udruzenjeId;
	    }
	
}
