<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book small-icons bk-color-brown"></i>Ново културно
				подручје
			</h1>

			<form:form action="sacuvaj-kulturno-podrucje"
				modelAttribute="podrucje" method="POST">
				<form:hidden path="podrucjeId" />

				<label class="margintop10" for="nazivPodrucja">Назив
					културног подручја</label>
				<form:input required="required" path="nazivPodrucja"
					cssClass="form-control" />

				<div class="row">
					<div class="col-25">
						<label for="ispUsl">Да ли је уметничко?</label>
					</div>
					<div class="col-75">
						<form:checkbox value="1"
							style="display:block;margin-left: 0%;width:3% ;"
							path="umetnicko" cssClass="form-control" />
						<form:hidden path="umetnicko" value="0" />
					</div>
				</div>
				

				<label class="margintop10" for="submit"></label>
				<input type="submit" value="Сачувај културно подручје"
					class="form-control btn-success mb-3 text-center">
			</form:form>
		</div>

		<div class="mb-4 mt-3">
			<input type="button" value="Одустани"
				class="form-control btn-info text-center"
				onclick="window.location.href='${pageContext.request.contextPath}/administracija/kulturna-podrucja'; return false;" />
		</div>

	</div>


	<%@ include file="footer.jsp"%>

</body>

</html>
