<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<c:url var="umetniciLink"
		value="/administracija/udruzenja/${udruzenje.pib}/umetnici">
	</c:url>
	<c:url var="infoLink"
		value="/administracija/udruzenja/${udruzenje.pib}">
	</c:url>
	<c:url var="podrucjaLink"
		value="/administracija/udruzenja/${udruzenje.pib}/kulturna-podrucja">
	</c:url>

	<div class="wrap-pad">
		<div class="container pt-3">
			<div class="row profile">

				<%@ include file="profil-udruzenja-import.jsp"%>

				<div class="col-md-9">
					<div class="profile-content">
						<p class="demo naslov">
							<b>Уметници</b>
						</p>
						<div class="text-center" style="overflow-x: auto;">
							<table class="table">
								<thead class="thead-dark">
									<tr>
										<th>Име</th>
										<th>Презиме</th>
										<th>Врста</th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${umetnici}" var="trenutniUmetnik">
										<c:url var="umetnikLink"
											value="/administracija/umetnici/${trenutniUmetnik.jmbg}"></c:url>
										<sec:authorize
											access="hasAuthority('Superadmin') OR (hasAuthority('Udruzenje') AND ${(trenutniUmetnik.proglasenOdUdruzenja.udruzenjeId == korisnik.udruzenje.udruzenjeId)})">
											<tr onclick="window.location='${umetnikLink}';">
										</sec:authorize>
										<sec:authorize
											access="(!hasAuthority('Superadmin') AND !hasAuthority('Udruzenje')) OR (hasAuthority('Udruzenje') AND ${(trenutniUmetnik.proglasenOdUdruzenja.udruzenjeId != korisnik.udruzenje.udruzenjeId)})">
											<tr>
										</sec:authorize>

										<td>${trenutniUmetnik.ime}</td>
										<td>${trenutniUmetnik.prezime}</td>
										<td>${trenutniUmetnik.tipUmetnika.nazivTipa}</td>
										</tr>

									</c:forEach>
								</tbody>
							</table>
						</div>
						<sec:authorize
							access="(hasAuthority('Superadmin') AND ${udruzenje.status ==1}) OR (hasAuthority('Udruzenje') AND ${(udruzenje.status ==1 && udruzenje.udruzenjeId == korisnik.udruzenje.udruzenjeId)})">
							<input type="button" value="Додај уметника"
								class="form-control btn-info text-center"
								onclick="window.location.href='${pageContext.request.contextPath}/administracija/umetnici/dodaj-novog-umetnika?udruzenjeID=${udruzenje.udruzenjeId}'; return false;" />
						</sec:authorize>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="footer.jsp"%>
</body>

</html>
