package registar_umetnika.regum.service.interfaces;

import java.util.List;

import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.Organ;

public interface OrganService {

	Organ vratiOrgan(int id);
	
	List<Organ> vratiOrgane();

	List<Organ> vratiOrganeIdNaziv();
}
