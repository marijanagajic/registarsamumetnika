package registar_umetnika.regum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import registar_umetnika.regum.dao.interfaces.KulturnoPodrucjeDAO;
import registar_umetnika.regum.dao.interfaces.MestoDAO;
import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.service.interfaces.KulturnoPodrucjeService;
import registar_umetnika.regum.service.interfaces.MestoService;

@Service
public class MestoServiceImpl implements MestoService {
	
	@Autowired
	private MestoDAO mestoDAO;
	
	@Override
	@Transactional
	public List<Mesto> vratiMesta() {
		return mestoDAO.vratiMesta();
	}

	@Override
	@Transactional
	public Mesto vratiMesto(int id) {
		return mestoDAO.vratiMestoById(id);
	}
	
}
