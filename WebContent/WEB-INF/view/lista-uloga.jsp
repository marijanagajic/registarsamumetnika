<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book"></i> Улоге корисника</h1>

			<table class="table">
				<thead class="thead-dark">
					<tr>
						<th class='text-center'>Назив улоге</th>
						<th class='text-center'>Опције</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="trenutnaUloga" items="${uloge}">
						<c:url var="prikazPoUloziLink"
							value="/administracija/korisnici-po-ulozi">
							<c:param name="ulogaId" value="${trenutnaUloga.ulogaId}" />
						</c:url>
						<tr>
							<td>${trenutnaUloga.nazivUloge}</td>
							<td><a href="${prikazPoUloziLink}">Прикажи кориснике по улози</a></td>
						<tr>
					</c:forEach>
				</tbody>
			</table>
		</div>

		

	</div>


	<%@ include file="footer.jsp"%>

</body>

</html>
