package registar_umetnika.regum.controller;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.Korisnik;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.TipUmetnika;
import registar_umetnika.regum.entity.Udruzenje;
import registar_umetnika.regum.entity.Umetnik;
import registar_umetnika.regum.service.interfaces.DelatnostService;
import registar_umetnika.regum.service.interfaces.KorisnikService;
import registar_umetnika.regum.service.interfaces.MestoService;
import registar_umetnika.regum.service.interfaces.UdruzenjeService;
import registar_umetnika.regum.service.interfaces.UmetnikService;
import registar_umetnika.regum.util.DelatnostEditor;
import registar_umetnika.regum.util.KorisnikEditor;
import registar_umetnika.regum.util.MestoEditor;
import registar_umetnika.regum.util.TipUmetnikaEditor;
import registar_umetnika.regum.util.UmetnikEditor;

@Controller
@RequestMapping("/administracija")
@SessionAttributes({ "umetnik", "tipoviUmetnika" })
public class DodavanjeUmetnikaController {

	@Autowired
	private UmetnikService umetnikService;
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private UdruzenjeService udruzenjeService;
	@Autowired
	private MestoService mestoService;
	@Autowired
	private DelatnostService delatnostService;
	@Autowired
	private TipUmetnikaEditor tipUmetnikaEditor;
	@Autowired
	private KorisnikEditor korisnikEditor;
	@Autowired
	private UmetnikEditor umetnikEditor;
	@Autowired
	private MestoEditor mestoEditor;
	@Autowired
	private DelatnostEditor delatnostEditor;

	@GetMapping("/umetnici/dodaj-novog-umetnika")
	public String dodajNovogUmetnikaUdruzenju(Model theModel, HttpSession session) {
		String username = (String) session.getAttribute("user");
		Korisnik k = korisnikService.vratiKorisnika("username", username);
		theModel.addAttribute("udruzenjeId", k.getUdruzenje().getUdruzenjeId());
		theModel.addAttribute("umetnik", new Umetnik());
		return "umetnik-forma";
	}

	@PostMapping("/umetnici/sacuvaj-novog-umetnika")
	public String sacuvajNovogUmetnika(@RequestParam("udruzenjeId") int udruzenjeId, @ModelAttribute(name = "umetnik") Umetnik u) {

		Umetnik proveraJMBG = umetnikService.vratiUmetnika("umetnikID", String.valueOf(u.getUmetnikId()));

		if(proveraJMBG == null || proveraJMBG.getUmetnikId() == u.getUmetnikId()) {
			u.setStatus(1);
			u.setProglasenOdUdruzenja(new Udruzenje(udruzenjeId));
			umetnikService.sacuvajUmetnika(u);
			
			/*if (c.getUdruzenje() != null) {
				c.setUmetnik(u);
				c.setDatum(new Date(System.currentTimeMillis()));
				clanstvoService.sacuvajNovoClanstvo(c);
			}*/
		} else {
			// nesto je promenjeno sto ne bi smelo
		}

		return "redirect:/administracija/umetnici/"+u.getJmbg();
	}

	@ModelAttribute("umetnik")
	public Umetnik umetnik() {
		return new Umetnik();
	}

	@ModelAttribute("tipoviUmetnika")
	public List<TipUmetnika> tipoviUmetnika() {
		return umetnikService.vratiTipoveUmetnika();
	}
	
	@ModelAttribute("delatnosti")
	public List<Delatnost> delatnosti() {
		return delatnostService.vratiDelatnosti();
	}
	
	@ModelAttribute("mesta")
	public List<Mesto> mesta() {
		return mestoService.vratiMesta();
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(TipUmetnika.class, this.tipUmetnikaEditor);
		binder.registerCustomEditor(Mesto.class, this.mestoEditor);
		binder.registerCustomEditor(Delatnost.class, this.delatnostEditor);
		binder.registerCustomEditor(Umetnik.class, this.umetnikEditor);
		binder.registerCustomEditor(Korisnik.class, this.korisnikEditor);
	}
}
