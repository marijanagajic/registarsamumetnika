<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags"
	prefix="sec"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book"></i> Корисници
			</h1>
			<c:if test="${notification != null}">
				<div class="alert alert-danger">
					<p>${notification}</p>
				</div>
			</c:if>
			<table class="table table-hover">
				<thead class="thead-dark ">
					<tr>
						<th>Име</th>
						<th>Презиме</th>
						<th class='text-center'>Корисничко име</th>
						<th class='text-center'>E-mail</th>
						<th class='text-center'>Улога</th>
						<th class='text-center'>Статус корисника</th>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="trenutniKorisnik" items="${korisnici}">

						<tr>
							<td>${trenutniKorisnik.ime}</td>
							<td>${trenutniKorisnik.prezime}</td>
							<td>${trenutniKorisnik.username}</td>
							<td>${trenutniKorisnik.email}</td>
							<td>${trenutniKorisnik.uloga.nazivUloge}</td>
							<td style="font-weight: bold;">${((trenutniKorisnik.enabled)? "Aктиван":"Неактиван")}</td>
						<tr>
					</c:forEach>
				</tbody>
			</table>
		</div>


	</div>

	<%@ include file="footer.jsp"%>

</body>

</html>
