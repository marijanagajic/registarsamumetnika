package registar_umetnika.regum.service.interfaces;

import java.util.List;

import registar_umetnika.regum.entity.Delatnost;

public interface DelatnostService {

	List<Delatnost> vratiDelatnosti();
	Delatnost vratiDelatnostById(int id);

}
