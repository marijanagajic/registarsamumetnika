package registar_umetnika.regum.util;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.Organ;
import registar_umetnika.regum.entity.TipUmetnika;
import registar_umetnika.regum.entity.Udruzenje;
import registar_umetnika.regum.service.interfaces.MestoService;
import registar_umetnika.regum.service.interfaces.OrganService;
import registar_umetnika.regum.service.interfaces.UdruzenjeService;
import registar_umetnika.regum.service.interfaces.UmetnikService;

@Component
public class OrganEditor extends PropertyEditorSupport {
	
	@Autowired
	private OrganService organService;
	
	public void setAsText(String id) {
		if (id.equals(String.valueOf(-1))) {
			this.setValue(null);
		} else {
		Organ o = this.organService.vratiOrgan(Integer.valueOf(id));
		this.setValue(o);
		}
	}
}
