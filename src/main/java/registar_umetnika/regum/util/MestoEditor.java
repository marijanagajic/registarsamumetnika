package registar_umetnika.regum.util;

import java.beans.PropertyEditorSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.TipUmetnika;
import registar_umetnika.regum.entity.Udruzenje;
import registar_umetnika.regum.service.interfaces.MestoService;
import registar_umetnika.regum.service.interfaces.UdruzenjeService;
import registar_umetnika.regum.service.interfaces.UmetnikService;

@Component
public class MestoEditor extends PropertyEditorSupport {
	
	@Autowired
	private MestoService mestoService;
	
	public void setAsText(String id) {
		Mesto m = this.mestoService.vratiMesto(Integer.valueOf(id));
		this.setValue(m);
	}
}
