<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="col-md-3">
	<div class="profile-sidebar text-center">

		<div class="profile-usertitle">
			<div class="profile-usertitle-name">${udruzenje.naziv}
				${umetnik.prezime}</div>
		</div>

		<div class="profile-usermenu">
			<ul class="nav flex-column">
				<li id="osnovne-info" class="nav-item"><a class="nav-link"
					href="${infoLink}"><i class="glyphicon glyphicon-home"></i>
						Основне информације</a></li>
				<li id="umetnici" class="nav-item"><a class="nav-link"
					href="${umetniciLink}"><i class="glyphicon glyphicon-usd"></i>
						Уметници</a></li>
				<li id="kulturna-podrucja" class="nav-item"><a class="nav-link"
					href="${podrucjaLink}"><i class="glyphicon glyphicon-usd"></i>
						Културна подручја</a></li>
			</ul>
		</div>

		<div class="profile-userbuttons">
			<sec:authorize
				access="((${udruzenje.status==1} AND (hasAuthority('Superadmin') OR hasAuthority('Ministarstvo'))) OR (hasAuthority('Udruzenje') AND ${(udruzenje.status==1 && udruzenje.udruzenjeId == korisnik.udruzenje.udruzenjeId)}))">
			<button
				onclick="window.location.href='${pageContext.request.contextPath}/administracija/udruzenja/izmeni-udruzenje?udruzenjeID=${udruzenje.udruzenjeId}'; return false;"
				type="button" class="btn btn-primary btn-sm">Измени податке</button>
				</sec:authorize>
			<sec:authorize
				access="(${udruzenje.status==1} AND (hasAuthority('Superadmin') OR hasAuthority('Ministarstvo')))">
				<div
					style="border: 1px solid cornflowerblue; background: aliceblue; margin-top: inherit;">
					<form action="deaktiviraj-udruzenje" method="POST">
						<input type="hidden" name="udruzenjeId" value="${udruzenje.udruzenjeId}" />
						<div class="row">
							<div class="col-25">
								<label style="padding-right: 0px; padding-left: 44%;"
									for="datumUkidanjaStatusa">Датум укидања статуса</label>
							</div>
							<div class="col-75">
								<input required="required" name="datumukidanjastatusa"
									type="date" cssClass="form-control" />
							</div>
						</div>
						<div class="row">
							<input type="hidden" name="${_csrf.parameterName}"
								value="${_csrf.token}" /> <input type="submit"
								value="Деактивирај" style="width: auto; margin-left: 30%;"
								class="form-control btn-primary btn-xl mt-2 mb-1">
						</div>
					</form>
				</div>
			</sec:authorize>

		
		</div>


	</div>
</div>
<script type="text/javascript">
	$("#osnovne-info").click(function() {
		$("#osnovne-info").toggleClass("active");
		$("#umetnici").removeClass("active");
		$("#kulturna-podrucja").removeClass("active");
	});

	$("#kulturna-podrucja").click(function() {
		$("#kulturna-podrucja").toggleClass("active");
		$("#osnovne-info").removeClass("active");
		$("#umetnici").removeClass("active");
	});

	$("#uplate").click(function() {
		$("#umetnici").toggleClass(active);
		$("#osnovne-info").removeClass("active");
		$("#kulturna-podrucja").removeClass("active");
	});
</script>



