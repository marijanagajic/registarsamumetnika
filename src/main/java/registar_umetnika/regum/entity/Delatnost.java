package registar_umetnika.regum.entity;

import java.sql.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Table(name = "delatnost")
@Data
@NoArgsConstructor
@AllArgsConstructor
public class Delatnost {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "iddelatnost", unique = true, nullable = false)
	private int delatnostId;

	@Column(name = "nazivdelatnost")
	private String nazivDelatnost;
}
