package registar_umetnika.regum.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import registar_umetnika.regum.dao.interfaces.DelatnostDAO;
import registar_umetnika.regum.dao.interfaces.KulturnoPodrucjeDAO;
import registar_umetnika.regum.dao.interfaces.MestoDAO;
import registar_umetnika.regum.dao.interfaces.OrganDAO;
import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.Organ;
import registar_umetnika.regum.service.interfaces.DelatnostService;
import registar_umetnika.regum.service.interfaces.KulturnoPodrucjeService;
import registar_umetnika.regum.service.interfaces.MestoService;
import registar_umetnika.regum.service.interfaces.OrganService;

@Service
public class OrganServiceImpl implements OrganService {
	
	@Autowired
	private OrganDAO organDAO;
	
	@Override
	@Transactional
	public Organ vratiOrgan(int id) {
		return organDAO.vratiOrgan(id);
	}
	
	@Override
	@Transactional
	public List<Organ> vratiOrgane() {
		return organDAO.vratiOrgane();
	}

	@Override
	@Transactional
	public List<Organ> vratiOrganeIdNaziv() {
		return organDAO.vratiOrganeIdNaziv();
	}

	
}
