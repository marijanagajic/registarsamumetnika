package registar_umetnika.regum.dao.interfaces;

import java.util.List;

import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;

public interface MestoDAO {

	List<Mesto> vratiMesta();
	Mesto vratiMestoById(int id);

}
