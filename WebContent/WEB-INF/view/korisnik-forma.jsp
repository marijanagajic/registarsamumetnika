<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book"></i> Кориснички налог
			</h1>
			<c:if test="${notification != null}">
				<div class="alert alert-danger">
					<p>${notification}</p>
				</div>
			</c:if>
			<form:form action="sacuvaj-korisnika" modelAttribute="korisnik"
				method="POST">
				<form:hidden path="korisnikId" />


				<div class="row">
					<div class="col-25">
						<label class="" for="ime">Име</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="ime" cssClass="form-control"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="prezime">Презиме</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="prezime"
							cssClass="form-control" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label class="" for="enabled">Статус корисника</label>
					</div>
					<div class="col-75">
						<form:select required="required" path="enabled"
							cssClass="form-control text-center">
							<form:option value="1" label="Активан"
								class="form-control text-center" />
							<form:option value="0" label="Нективан"
								class="form-control text-center" />
						</form:select>
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="username">Корисничко име</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="username" type="username"
							cssClass="form-control" autocomplete="false"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"  />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="password">Лозинка</label>
					</div>
					<div class="col-75">
						<form:password path="password" autocomplete="false"
							pattern="^(?=.*[а-яa-zћђљњџ])(?=.*[А-ЯA-ZЏЋЂЉЊЏ])(?=.*\d)(?=.*[#$^+.=!*()@%&]).{6,}$"
							oninvalid="this.setCustomValidity('Лозинка мора садржати најмање један број, један специјалан знак, једно велико и једно мало слово и најмање 6 карактера!!')"
							oninput="setCustomValidity('')"
							cssClass="form-control" placeholder="Уписати само уколико се мења лозинка, уколико оставите празно поље важи постојећа лозинка." />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="email">e-mail</label>
					</div>
					<div class="col-75">
						<form:input path="email" required="required" pattern="[a-zA-Z0-9_\-\.]*@[a-z\.]*\.[a-z]{2,3}"
							oninvalid="this.setCustomValidity('Адреса електронске поште није правилно унета!')"
							oninput="setCustomValidity('')"
							cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="uloga">Улога</label>
					</div>
					<div class="col-75">
						<form:select id="ulogaCombo" required="required" path="uloga"
							onchange="proveraUloga(this.value);"
							cssClass="form-control text-center"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" >
							<form:options items="${uloge}" itemLabel="nazivUloge"
								itemValue="ulogaId" class="form-control text-center" />
						</form:select>
					</div>
				</div>
				<div id="udruzenjeCombo" class="row">
					<div class="col-25">
						<label class="" for="udruzenje">Удружење</label>
					</div>
					<div class="col-75">
						<form:select id="udruzenjeSelect"  path="udruzenje"
							cssClass="form-control text-center">
							<form:option value="-1">&nbsp;</form:option>
							<form:options items="${udruzenja}" itemLabel="naziv"
								itemValue="udruzenjeId" class="form-control text-center" />
						</form:select>
					</div>
				</div>
				<div id="organCombo" class="row">
					<div class="col-25">
						<label class="" for="organ">Орган</label>
					</div>
					<div class="col-75">
						<form:select id="organSelect"  path="organ"
							cssClass="form-control text-center">
							<form:option value="-1">&nbsp;</form:option>
							<form:options items="${organi}" itemLabel="naziv"
								itemValue="organId" class="form-control text-center" />
						</form:select>
					</div>
				</div>
			

				<label class="" for="submit"></label>
				<input type="submit" value="Сачувај"
					class="form-control btn-success text-center mb-3">
			</form:form>

			<div class="mb-4 mt-3">
				<input type="button" value="Одустани"
					class="form-control btn-info text-center"
					onclick="window.location.href='${pageContext.request.contextPath}/administracija/korisnici'; return false;" />
			</div>
		</div>
	</div>

	<%@ include file="footer.jsp"%>

	<script type="text/javascript">
		var id = document.getElementById("ulogaCombo").value;
		if (id == 3) {
			document.getElementById("organSelect").options.selectedIndex = "-1";
			document.getElementById("udruzenjeCombo").style.display = "block";
			document.getElementById("organCombo").style.display = "none";
		} else if (id == 4) {
			document.getElementById("udruzenjeSelect").options.selectedIndex = "-1";
			document.getElementById("organCombo").style.display = "block";
			document.getElementById("udruzenjeCombo").style.display = "none";
		} else {
			document.getElementById("udruzenjeSelect").options.selectedIndex = "-1";
			document.getElementById("organSelect").options.selectedIndex = "-1";
			document.getElementById("organCombo").style.display = "none";
			document.getElementById("udruzenjeCombo").style.display = "none";
		}
		function proveraUloga(id) {
			if (id == 3) {
				document.getElementById("organSelect").options.selectedIndex = "-1";
				document.getElementById("udruzenjeCombo").style.display = "block";
				document.getElementById("organCombo").style.display = "none";
			} else if (id == 4) {
				document.getElementById("udruzenjeSelect").options.selectedIndex = "-1";
				document.getElementById("organCombo").style.display = "block";
				document.getElementById("udruzenjeCombo").style.display = "none";
			} else {
				document.getElementById("udruzenjeSelect").options.selectedIndex = "-1";
				document.getElementById("organSelect").options.selectedIndex = "-1";
				document.getElementById("organCombo").style.display = "none";
				document.getElementById("udruzenjeCombo").style.display = "none";
			}
		}
	</script>
</body>

</html>
