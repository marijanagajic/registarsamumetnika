<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<c:url var="uplateLink"
		value="/administracija/umetnici/${umetnik.jmbg}/uplate">
	</c:url>
	<c:url var="referenceLink"
		value="/administracija/umetnici/${umetnik.jmbg}/reference">
	</c:url>
	<c:url var="infoLink" value="/administracija/umetnici/${umetnik.jmbg}">
	</c:url>

	<div class="wrap-pad">
		<div class="container pt-3">
			<div class="row profile">

				<%@ include file="profil-umetnika-import.jsp" %>

				<div class="col-md-9">
					<div class="profile-content">
						<ul class="demo">
							<li class="demo" onclick="window.location='${pageContext.request.contextPath}/administracija/udruzenja/${umetnik.proglasenOdUdruzenja.pib}';">Проглашен од удружења: ${umetnik.proglasenOdUdruzenja.naziv}</li>
							<li class="demo">Датум рођења: ${umetnik.datumRodjenja}</li>
							<li class="demo">Име родитеља: ${umetnik.imeRoditelja}</li>
							<li class="demo">ЈМБГ: ${umetnik.jmbg}</li>
							<li class="demo">Место рођења: ${umetnik.mestoRodjenja.nazivMesto}</li>
							<li class="demo">Општина рођења: ${umetnik.opstinaRodjenja}</li>
							<li class="demo">Држављанство: ${umetnik.drzavljanstvo}</li>
							<li class="demo">Пребивалиште: ${umetnik.prebivaliste}</li>
							<li class="demo">Адреса: ${umetnik.ulica} ${umetnik.broj}</li>
							<li class="demo">Број телефона: ${umetnik.brojTelefona}</li>
							<li class="demo">Делатност: ${umetnik.delatnost.nazivDelatnost}</li>
							<li class="demo">Датум почетка обављања делатности: ${umetnik.datumPocetkaObavljanjaDelatnosti}</li>
							<li class="demo">Број акта: ${umetnik.brojAkta}</li>
							<li class="demo">Статус запослења: ${umetnik.statusZaposlenja}</li>
							<li class="demo">Место запослења: ${umetnik.mestoZaposlenja.nazivMesto}</li>
							<li class="demo">Датум престанка обављања делатности: ${umetnik.datumPrestankaObavljanjaDelatnosti}</li>
							<li class="demo">Датум уписа: ${umetnik.datumUpisa}</li>
							<li class="demo">Статус: ${((umetnik.status==1)? "Важећи":"Укинут")}</li>
							<li class="demo" style="${((umetnik.status==1)? "display:none":"")}">Датум укидања статуса самосталног уметника у области културе: ${umetnik.datumukidanjastatusa}</li>
							<li class="demo">Напомена: ${umetnik.napomena}</li>
							
						</ul>
					</div>
					
				</div>
			</div>
		</div>
	</div>

	<%@ include file="footer.jsp"%>
</body>

</html>
