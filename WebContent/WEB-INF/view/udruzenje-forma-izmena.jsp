<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book"></i>Измени податке удружења
			</h1>

			<form:form action="sacuvaj-izmene" modelAttribute="udruzenje"
				method="POST">
				<form:hidden path="udruzenjeId" />
				
				<form:hidden path="status"/>


				<div class="row">
					<div class="col-25">
						<label class="" for="maticniBroj">Матични број</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="maticniBroj"
							cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="pib">ПИБ</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="pib" cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="naziv">Назив удружења</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="naziv"
							cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="sediste">Седиште</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="sediste"
							cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="adresa">Адреса</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="adresa"
							cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="datumOsnivanja">Датум оснивања</label>
					</div>
					<div class="col-75">
						<form:input required="required" type="date" path="datumOsnivanja"
							cssClass="form-control" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="zastupnik">Заступник</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="zastupnik"
							cssClass="form-control" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label class="" for="url">URL</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="url"
							cssClass="form-control" />
					</div>
				</div>
				<sec:authorize access="hasAuthority('Superadmin') OR hasAuthority('Ministarstvo')">
					<div class="row">
						<div class="col-25">
							<label class="" for="registarskibroj">Регистарски број</label>
						</div>
						<div class="col-75">
							<form:input required="required" path="registarskiBroj"
								cssClass="form-control" />
						</div>
					</div>
				</sec:authorize>

				<label class="margintop10" for="submit"></label>
				<input type="submit" value="Сачувај измене"
					class="form-control btn-success mb-3 text-center">
			</form:form>
		</div>

		<div class="mb-4 mt-3">
			<input type="button" value="Одустани"
				class="form-control btn-info text-center"
				onclick="window.location.href='${pageContext.request.contextPath}/administracija/udruzenja/${udruzenje.pib}'; return false;" />
		</div>

	</div>


	<%@ include file="footer.jsp"%>

</body>

</html>
