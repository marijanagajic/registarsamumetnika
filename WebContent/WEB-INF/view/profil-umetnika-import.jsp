<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<div class="col-md-3">
	<div class="profile-sidebar text-center">
		

		<div class="profile-usertitle">
			<div class="profile-usertitle-name">${umetnik.ime}
				${umetnik.prezime}</div>
			<div class="profile-usertitle-job">${umetnik.tipUmetnika.nazivTipa}</div>
		</div>


		<div class="profile-usermenu">
			<ul class="nav flex-column">
				<li id="osnovne-info" class="nav-item"><a class="nav-link"
					href="${infoLink}"><i class="glyphicon glyphicon-home"></i>
						Основне информације</a></li>
				<sec:authorize access="hasAuthority('Superadmin') OR hasAuthority('Umetnik') OR hasAuthority('Lokalna samouprava')">
				<li id="uplate" class="nav-item"><a class="nav-link" href="${uplateLink}"><i
						class="glyphicon glyphicon-usd"></i> Уплате</a></li>
				</sec:authorize>
				<li id="reference" class="nav-item"><a class="nav-link"
					href="${referenceLink}"><i class="glyphicon glyphicon-flag"></i>
						Референце</a></li>
			</ul>
		</div>
		<div class="profile-userbuttons">
			<sec:authorize access="hasAuthority('Superadmin') OR hasAuthority('Umetnik')">
			<button
				onclick="window.location.href='${pageContext.request.contextPath}/administracija/umetnici/${umetnik.jmbg}/dodaj-referencu'; return false;"
				type="button" class="form-control btn btn-primary btn-sm">Додај
				референцу</button>
			</sec:authorize>
			<sec:authorize access="hasAuthority('Superadmin') OR hasAuthority('Umetnik')">
			<button
				onclick="window.location.href='${pageContext.request.contextPath}/administracija/umetnici/izmeni-podatke?jmbg=${umetnik.jmbg}'; return false;"
				type="button" class="form-control btn btn-primary btn-sm">Измени
				податке</button>
			</sec:authorize>
			<sec:authorize access="(hasAuthority('Udruzenje') AND ${umetnik.korisnik.korisnikId ==null})">
			<button
				onclick="window.location.href='${pageContext.request.contextPath}/administracija/korisnici/dodaj-novog-korisnika?umetnikID=${umetnik.umetnikId}'; return false;"
				type="button" class="form-control btn btn-primary btn-sm">Направи налог уметнику</button>
			</sec:authorize>
			<sec:authorize access="(hasAuthority('Udruzenje') AND ${umetnik.korisnik.korisnikId !=null})">
			Корисник има креиран налог
			</sec:authorize>
			<sec:authorize access="(${umetnik.status==1} AND hasAuthority('Superadmin')) OR (hasAuthority('Udruzenje') AND ${(umetnik.status==1 && umetnik.proglasenOdUdruzenja.udruzenjeId == korisnik.udruzenje.udruzenjeId)})">
			<div style="border: 1px solid cornflowerblue;background: aliceblue;margin-top: inherit;">
			<form action="deaktiviraj-umetnika" method="POST">
			<input type="hidden" name="umetnikID" value="${umetnik.umetnikId}"/>
			<div class="row">
					<div class="col-25">
						<label style="padding-right: 0px;padding-left: 44%;" for="datumUkidanjaStatusa">Датум укидања статуса</label>
					</div>
					<div class="col-75">
						<input required="required" name="datumUkidanjaStatusa" type="date"
							cssClass="form-control"  />
					</div>
				</div>
				<div class="row">
					<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}"/>
					<input type="submit" value="Деактивирај" style="width: auto;margin-left: 30%;"
						class="form-control btn-primary btn-xl mt-2 mb-1">
				</div>
			</form>
			</div>
			</sec:authorize>
		</div>
	</div>
</div>

<script type="text/javascript">

	
	$("#osnovne-info").click(function(){
		$("#osnovne-info").toggleClass("active");	
		$("#uplate").removeClass("active");
		$("#reference").removeClass("active");
	});
	
	$("#reference").click(function(){
		$("#reference").toggleClass("active");	
		$("#osnovne-info").removeClass("active");
		$("#uplate").removeClass("active");
	});
	
	$("#uplate").click(function(){
		$("#uplate").toggleClass(active);	
		$("#osnovne-info").removeClass("active");
		$("#reference").removeClass("active");
	});
	
</script>



