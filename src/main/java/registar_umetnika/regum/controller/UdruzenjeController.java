package registar_umetnika.regum.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import registar_umetnika.regum.entity.Korisnik;
import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.PripadnostUdruzenja;
import registar_umetnika.regum.entity.Udruzenje;
import registar_umetnika.regum.entity.Umetnik;
import registar_umetnika.regum.service.interfaces.KorisnikService;
import registar_umetnika.regum.service.interfaces.KulturnoPodrucjeService;
import registar_umetnika.regum.service.interfaces.PripadnostUdruzenjaService;
import registar_umetnika.regum.service.interfaces.UdruzenjeService;
import registar_umetnika.regum.service.interfaces.UmetnikService;
import registar_umetnika.regum.util.KulturnoPodrucjeEditor;
import registar_umetnika.regum.util.UdruzenjeEditor;

@Controller
@RequestMapping("/administracija")
@Transactional
public class UdruzenjeController {
	
	@Autowired
	private UdruzenjeService udruzenjeService;
	@Autowired
	private UmetnikService umetnikService;
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private PripadnostUdruzenjaService pripadnostUdruzenjaService;
	@Autowired
	private KulturnoPodrucjeService kulturnoPodrucjeService;
	@Autowired
	private KulturnoPodrucjeEditor kulturnoPodrucjeEditor;
	@Autowired
	private UdruzenjeEditor udruzenjeEditor;
	
	
	@RequestMapping("/udruzenja")
	public String prikaziUdruzenja(Model theModel) {
		List<Udruzenje> udruzenja = udruzenjeService.vratiAktivnaUdruzenja();
		theModel.addAttribute("udruzenja", udruzenja);
		return "lista-udruzenja";
	}
	
	@RequestMapping("/sva-udruzenja")
	public String prikaziSvaUdruzenja(Model theModel) {
		List<Udruzenje> udruzenja= udruzenjeService.vratiUdruzenja();
		theModel.addAttribute("udruzenja", udruzenja);

		return "lista-udruzenja-svi";
	}
	
	@RequestMapping("/udruzenja/{pib}")
	public String prikaziUdruzenje(@PathVariable("pib") int pib, Model theModel, HttpSession session) {
		Udruzenje u = udruzenjeService.vratiUdruzenje("pib", pib + "");
		theModel.addAttribute("udruzenje", u);
		String username = (String) session.getAttribute("user");
		Korisnik k = korisnikService.vratiKorisnika("username", username);
		theModel.addAttribute("korisnik", k);
		
		List<KulturnoPodrucje> podrucjaKojimaPripada = new ArrayList<KulturnoPodrucje>();
		List<PripadnostUdruzenja> pripadnosti = pripadnostUdruzenjaService.vratiPripadnostiPoUdruzenju(u.getUdruzenjeId());
		
		for(PripadnostUdruzenja pu : pripadnosti) {
			KulturnoPodrucje kp = kulturnoPodrucjeService.vratiKulturnoPodrucje(pu.getKulturnoPodrucje().getPodrucjeId());
			podrucjaKojimaPripada.add(kp);
		}
		
		theModel.addAttribute("podrucjaKojimaPripada", podrucjaKojimaPripada);
		theModel.addAttribute("pripadnosti", pripadnosti);
		
		return "profil-udruzenja";
	}
	
	@RequestMapping("/udruzenja/{pib}/umetnici")
	public String prikaziUmetnikeUdruzenja(@PathVariable("pib") int pib, Model theModel, HttpSession session) {
		Udruzenje u = udruzenjeService.vratiUdruzenje("pib", pib + "");
		theModel.addAttribute("udruzenje", u);
		String username = (String) session.getAttribute("user");
		Korisnik k = korisnikService.vratiKorisnika("username", username);
		theModel.addAttribute("korisnik", k);
		theModel.addAttribute("umetnici", umetnikService.vratiUmetnikeByIdUdruzenja(u.getUdruzenjeId()));
		
		return "profil-udruzenja-umetnici";
	}
	
	@RequestMapping("/udruzenja/{pib}/kulturna-podrucja")
	public String prikaziKulturnaPodrucjaUdruzenja(@PathVariable("pib") int pib, Model theModel, HttpSession session) {
		String username = (String) session.getAttribute("user");
		Korisnik k = korisnikService.vratiKorisnika("username", username);
		theModel.addAttribute("korisnik", k);
		Udruzenje u = udruzenjeService.vratiUdruzenje("pib", pib + "");
		theModel.addAttribute("udruzenje", u);
		List<PripadnostUdruzenja> podrucja = pripadnostUdruzenjaService.vratiPripadnostiPoUdruzenju(u.getUdruzenjeId());
		
		theModel.addAttribute("podrucja", podrucja);
		
		return "profil-udruzenja-kulturna-podrucja";
	}
	
	@GetMapping("/udruzenja/{pib}/deaktiviraj-kulturno-podrucje-udruzenju")
	public String obrisiPodrucjeUdruzenju(@PathVariable("pib") int pib, Model theModel,
			@RequestParam("udruzenjeID") int udruzenjeId, 
			@RequestParam("podrucjeID") int podrucjeId, 
			@RequestParam(name="datumUkidanjaStatusa") Date datumUkidanjaStatusa) {
		
		Udruzenje u = udruzenjeService.vratiUdruzenje(udruzenjeId);
		PripadnostUdruzenja pu = pripadnostUdruzenjaService.vratiPripadnostUdruzenja(udruzenjeId, podrucjeId);
		pu.setDatumUkidanjaStatusa(datumUkidanjaStatusa);
		pripadnostUdruzenjaService.sacuvaj(pu);
		
		return "redirect:/administracija/udruzenja/" + u.getPib()+"/kulturna-podrucja";
	}
	
	@RequestMapping(value="/udruzenja/{pib}/deaktiviraj-udruzenje", method=RequestMethod.POST)
	public String obrisiUdruzenje(@RequestParam("udruzenjeId") int id, @RequestParam("datumukidanjastatusa") Date datum) {
		Udruzenje u = udruzenjeService.vratiUdruzenje(id);
		u.setStatus(0);
		u.setDatumukidanjastatusa(datum);
		udruzenjeService.sacuvajUdruzenje(u);
		List<PripadnostUdruzenja> pu = pripadnostUdruzenjaService.vratiPripadnostiPoUdruzenju(id);
		for (PripadnostUdruzenja pripadnost : pu) {
			if (pripadnost.getDatumUkidanjaStatusa() == null) {
				pripadnost.setDatumUkidanjaStatusa(datum);
				pripadnostUdruzenjaService.sacuvaj(pripadnost);
			}
		}
		
		return "redirect:/administracija/udruzenja/"+u.getPib();
	}
	
	@GetMapping("/udruzenja/izmeni-udruzenje")
	public String izmeniUdruzenje(@RequestParam("udruzenjeID") int id,
			Model theModel) {
		Udruzenje u = udruzenjeService.vratiUdruzenje(id);
		
		theModel.addAttribute("udruzenje", u);
		
		return "udruzenje-forma-izmena";
	}
	
	@PostMapping("/udruzenja/sacuvaj-izmene")
	public String sacuvajIzmene(@ModelAttribute(name="udruzenje") Udruzenje u) {
		udruzenjeService.sacuvajUdruzenje(u);
		return "redirect:/administracija/udruzenja/"+u.getPib();
	}
	
	@GetMapping("/udruzenja/clanovi")
	public String clanoviUdruzenja(@RequestParam("udruzenjeID") int id, Model theModel) {
		
		// inicijalizacija liste umetnika
		
		// dodavanje liste u model
		
		return "clanovi-udruzenja";
	}

	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(KulturnoPodrucje.class, this.kulturnoPodrucjeEditor);
		binder.registerCustomEditor(Udruzenje.class, this.udruzenjeEditor);
	}
}
