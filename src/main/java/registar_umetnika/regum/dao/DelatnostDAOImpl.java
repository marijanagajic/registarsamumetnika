package registar_umetnika.regum.dao;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.query.Query;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import registar_umetnika.regum.dao.interfaces.DelatnostDAO;
import registar_umetnika.regum.dao.interfaces.KulturnoPodrucjeDAO;
import registar_umetnika.regum.dao.interfaces.MestoDAO;
import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;

@Repository
public class DelatnostDAOImpl implements DelatnostDAO {

	@Autowired
	private SessionFactory sessionFactory;
	
	@Override
	public List<Delatnost> vratiDelatnosti() {
		Session currentSession = sessionFactory.getCurrentSession();
		Query theQuery = currentSession.createQuery("from Delatnost", Delatnost.class);
		return theQuery.getResultList();
	}

	@Override
	public Delatnost vratiDelatnostById(int id) {
		Session currentSession = sessionFactory.getCurrentSession();
		Query theQuery = currentSession.createQuery("from Delatnost d where d.delatnostId="+id, Delatnost.class);
		return (Delatnost)theQuery.getSingleResult();
	}
	
		
}
