<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>

<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad">
		<p class="demo naslov">
			<b>ПОДАЦИ О УМЕТНИКУ</b>
		</p>
		<p style="text-align: center">Молимо Вас попуните све податке</p>

		<div class="container">
			<form:form action="sacuvaj-novog-umetnika" modelAttribute="umetnik"
				method="POST">
				<input type="hidden" name="udruzenjeId" value="${udruzenjeId}"/>
				<form:hidden path="umetnikId" />
				<form:hidden path="korisnik.korisnikId" />

				<div class="row">
					<div class="col-25">
						<label for="ime">Име</label>
					</div>
					<div class="col-75">
						<form:input required="required" type="text" path="ime" cssClass="form-control"
							 oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label for="prezime">Презиме</label>
					</div>
					<div class="col-75">
						<form:input required="required" type="text" path="prezime" cssClass="form-control"
							 oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="imeRoditelja">Име родитеља</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="imeRoditelja" cssClass="form-control"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="jmbg">ЈМБГ</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="jmbg" cssClass="form-control"
							 pattern="[0-9]{13}" oninvalid="this.setCustomValidity('Јмбг мора имати 13 цифара!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="datumrodjenja">Датум рођења</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="datumRodjenja" type="date"
							cssClass="form-control" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="mrodj">Место рођења</label>
					</div>
					<div class="col-75">
						<form:select required="required" path="mestoRodjenja"
							cssClass="form-control text-center" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" >
							<form:options items="${mesta}" itemLabel="nazivMesto"
								itemValue="mestoId" cssClass="form-control text-center" />
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="opstina">Општина рођења</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="opstinaRodjenja" cssClass="form-control"
							 oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="lname">Држављанство</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="drzavljanstvo" cssClass="form-control"
							 oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="prebiv">Пребивалиште</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="prebivaliste" cssClass="form-control"
							 oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="ulica">Улица</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="ulica" cssClass="form-control"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="broj">Број</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="broj" cssClass="form-control"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="brtel">Број телефона</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="brojTelefona" cssClass="form-control"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="delatn">Делатност</label>
					</div>
					<div class="col-75">
						<form:select required="required" path="delatnost"
							cssClass="form-control text-center" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')">
							<form:options items="${delatnosti}" itemLabel="nazivDelatnost"
								itemValue="delatnostId" cssClass="form-control text-center" />
						</form:select>
					</div>
					</div>
				
				<div class="row">
					<div class="col-25">
						<label for="dpod">Датум почетка обављања делатности</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="datumPocetkaObavljanjaDelatnosti" type="date"
							cssClass="form-control" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="brAkta">Број акта</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="brojAkta" cssClass="form-control"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="status">Статус запослења</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="statusZaposlenja" cssClass="form-control"
							oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="mestoZap">Место запослења</label>
					</div>
					<div class="col-75">
						<form:select required="required" path="mestoZaposlenja"
							cssClass="form-control text-center" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')">
							<form:options items="${mesta}" itemLabel="nazivMesto"
								itemValue="mestoId" cssClass="form-control text-center" />
						</form:select>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="dpod">Датум престанка обављања делатности</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="datumPrestankaObavljanjaDelatnosti" type="date"
							cssClass="form-control" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>
				<div class="row">
					<div class="col-25">
						<label for="ispUsl">Испуњеност услова</label>
					</div>
					<div class="col-75">
						<form:checkbox value="1" style="display:block;margin-left: 2%;width:3% ;${((korisnik.uloga.ulogaId==5)? 'pointer-events:none;':'')}"  path="ispunjenostUslova" cssClass="form-control" />
						<form:hidden path="ispunjenostUslova" value="0"/>
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label for="ispUsl">Датум уписа</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="datumUpisa" style="${((korisnik.uloga.ulogaId==5)? 'pointer-events:none;background-color: #f2f2f2;':'')}" type="date" cssClass="form-control" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')"/>
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label for="ispUsl">Тип уметника</label>
					</div>
					<div class="col-75">
						<form:select required="required" path="tipUmetnika" style="${((korisnik.uloga.ulogaId==5)? 'pointer-events:none;background-color: #f2f2f2;':'')}"
							cssClass="form-control text-center" oninvalid="this.setCustomValidity('Обавезно поље!')"
							oninput="setCustomValidity('')">
							<form:options items="${tipoviUmetnika}" itemLabel="nazivTipa"
								itemValue="tipUmetnikaId" cssClass="form-control text-center" />
						</form:select>
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label for="subject">Напомена</label>
					</div>
					<div class="col-75">
						<form:textarea path="napomena" 
							style="height: 200px"></form:textarea>
					</div>
				</div>

				<div class="row">
					<label class="margintop10" for="submit"></label> 
					<input type="submit" name="pocni" value="Потврди"
						class="form-control btn-primary btn-xl mt-2 mb-1">
				</div>
				<div class="row">
					<input type="button" value="Одустани"
						class="form-control btn-info text-center mb-4 mt-3"
						onclick="window.location.href='${pageContext.request.contextPath}/profil'; return false;" />
				</div>
			</form:form>
		</div>
	</div>

	<%@ include file="footer.jsp"%>

</body>

</html>
