<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">
			<h1>
				<i class="fa fa-book"></i> Креирање корисничког налога
			</h1>
			<c:if test="${notification != null}">
				<div class="alert alert-danger">
					<p>${notification}</p>
				</div>
			</c:if>

			<form:form action="sacuvaj-korisnika" modelAttribute="korisnik"
				method="POST">
				<form:hidden path="korisnikId" />
				<form:hidden path="ime" />
				<form:hidden path="prezime" />
				<form:hidden path="enabled" value="1" />
				<form:hidden path="uloga" value="5" />

				<div class="row">
					<div class="col-25">
						<label class="" for="username">Корисничко име</label>
					</div>
					<div class="col-75">
						<form:input required="required" path="username" type="username"
							cssClass="form-control" autocomplete="false" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="password">Лозинка</label>
					</div>
					<div class="col-75">
						<form:password required="required" path="password"
							autocomplete="false" cssClass="form-control"
							pattern="^(?=.*[а-яa-zћђљњџ])(?=.*[А-ЯA-ZЏЋЂЉЊЏ])(?=.*\d)(?=.*[#$^+.=!*()@%&]).{6,}$"
							oninvalid="this.setCustomValidity('Лозинка мора садржати најмање један број, један специјалан знак, једно велико и једно мало слово и најмање 6 карактера!!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="email">e-mail</label>
					</div>
					<div class="col-75">
						<form:input path="email" required="required"
							cssClass="form-control"
							pattern="[a-zA-Z0-9_\-\.]*@[a-z\.]*\.[a-z]{2,3}"
							oninvalid="this.setCustomValidity('Адреса електронске поште није правилно унета!')"
							oninput="setCustomValidity('')" />
					</div>
				</div>

				<div class="row">
					<div class="col-25">
						<label class="" for="uloga">Улога</label>
					</div>
					<div class="col-75">
						<form:select required="required" disabled="true" path="uloga"
							cssClass="form-control text-center">
							<form:options items="${uloge}" itemLabel="nazivUloge"
								itemValue="ulogaId" class="form-control text-center" />
						</form:select>
					</div>
				</div>

				<label class="" for="submit"></label>
				<input type="submit" value="Сачувај"
					class="form-control btn-success text-center mb-3">
			</form:form>

			<div class="mb-4 mt-3">
				<input type="button" value="Одустани"
					class="form-control btn-info text-center"
					onclick="window.location.href='${pageContext.request.contextPath}/administracija/korisnici'; return false;" />
			</div>
		</div>
	</div>

	<%@ include file="footer.jsp"%>


</body>

</html>
