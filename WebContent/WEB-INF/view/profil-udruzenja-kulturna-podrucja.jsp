<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">

<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<c:url var="umetniciLink"
		value="/administracija/udruzenja/${udruzenje.pib}/umetnici">
	</c:url>
	<c:url var="infoLink"
		value="/administracija/udruzenja/${udruzenje.pib}">
	</c:url>
	<c:url var="podrucjaLink"
		value="/administracija/udruzenja/${udruzenje.pib}/kulturna-podrucja">
	</c:url>

	<div class="wrap-pad">
		<div class="container pt-3">
			<div class="row profile">

				<%@ include file="profil-udruzenja-import.jsp"%>

				<div class="col-md-9">
					<div class="profile-content">
						<p class="demo naslov">
							<b>Културна подручја</b>
						</p>
						<div class="text-center" style="overflow-x: auto;">
							<table class="table">
								<thead class="thead-dark">
									<tr>
										<th>Назив</th>
										<th>Датум укидања статуса</th>
										<th></th>
									</tr>
								</thead>
								<tbody>
									<c:forEach items="${podrucja}" var="trenutnoPodrucje">
										<tr>
											<form action=deaktiviraj-kulturno-podrucje-udruzenju
												method="GET">
												<input type="hidden" name="udruzenjeID"
													value="${udruzenje.udruzenjeId}" /> <input type="hidden"
													name="podrucjeID"
													value="${trenutnoPodrucje.kulturnoPodrucje.podrucjeId}" />
												<td>${trenutnoPodrucje.kulturnoPodrucje.nazivPodrucja}</td>
												<td><sec:authorize
														access="hasAuthority('Superadmin') OR hasAuthority('Ministarstvo') AND ${trenutnoPodrucje.datumUkidanjaStatusa == null}">
														<input required="required" type="date"
															name="datumUkidanjaStatusa" cssClass="form-control" />
													</sec:authorize> <sec:authorize
														access="(hasAuthority('Superadmin') OR hasAuthority('Ministarstvo') AND ${trenutnoPodrucje.datumUkidanjaStatusa != null}) OR (!hasAuthority('Superadmin') AND !hasAuthority('Ministarstvo'))">
														<label>${trenutnoPodrucje.datumUkidanjaStatusa}</label>
													</sec:authorize></td>
												<td><sec:authorize
														access="hasAuthority('Superadmin') OR hasAuthority('Ministarstvo') AND ${trenutnoPodrucje.datumUkidanjaStatusa == null}">
														<input type="submit" value="Деактивирај">
													</sec:authorize> <sec:authorize
														access="(hasAuthority('Superadmin') OR hasAuthority('Ministarstvo') AND ${trenutnoPodrucje.datumUkidanjaStatusa != null}) OR (!hasAuthority('Superadmin') AND !hasAuthority('Ministarstvo'))">
														<label>${((trenutnoPodrucje.datumUkidanjaStatusa != null)?"Укинут":"Важећи")}</label>
													</sec:authorize>
												</td>
											</form>
										</tr>

									</c:forEach>
								</tbody>
							</table>
						</div>

						<sec:authorize
							access="(hasAuthority('Superadmin') AND ${udruzenje.status ==1}) ">
							<input type="button" value="Додај културно подручје"
								class="form-control btn-info text-center"
								onclick="window.location.href='${pageContext.request.contextPath}/administracija/udruzenja/dodaj-kulturno-podrucje?udruzenjeID=${udruzenje.udruzenjeId}'; return false;" />
						</sec:authorize>
					</div>
				</div>
			</div>
		</div>
	</div>

	<%@ include file="footer.jsp"%>
</body>

</html>
