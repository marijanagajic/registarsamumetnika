<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<c:url var="umetniciLink"
		value="/administracija/udruzenja/${udruzenje.pib}/umetnici">
	</c:url>
	<c:url var="infoLink"
		value="/administracija/udruzenja/${udruzenje.pib}">
	</c:url>
	<c:url var="podrucjaLink"
		value="/administracija/udruzenja/${udruzenje.pib}/kulturna-podrucja">
	</c:url>

	<div class="wrap-pad">
		<div class="container pt-3">
			<div class="row profile">

				<%@ include file="profil-udruzenja-import.jsp"%>

				<div class="col-md-9">
					<div class="profile-content">
						<ul class="demo">
							<li class="demo">ПИБ: ${udruzenje.pib}</li>
							<li class="demo">Матични број: ${udruzenje.maticniBroj}</li>
							<li class="demo">Седиште: ${udruzenje.sediste}</li>
							<li class="demo">Адреса: ${udruzenje.adresa}</li>
							<li class="demo">Датум оснивања: ${udruzenje.datumOsnivanja}</li>
							<li class="demo">Заступник: ${udruzenje.zastupnik}</li>
							<sec:authorize
							access="hasAuthority('Superadmin') OR hasAuthority('Ministarstvo')" >
							<li class="demo">Регистарски број: ${udruzenje.registarskiBroj}</li>
							</sec:authorize>
							<li class="demo">URL: ${udruzenje.url}</li>
							<li class="demo">Статус: ${((udruzenje.status==1)? "Важећи":"Укинут")}</li>
							<li class="demo" style="${((udruzenje.status==1)? "display:none":"")}">Датум укидања статуса: ${udruzenje.datumukidanjastatusa}</li>
						</ul>

					</div>
				</div>
			</div>
		</div>
	</div>
	</div>

	<%@ include file="footer.jsp"%>
</body>

</html>
