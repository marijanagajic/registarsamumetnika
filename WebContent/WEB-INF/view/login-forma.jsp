<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport"
	content="width=device-width, initial-scale=1, maximum-scale=1">
<meta name="description" content="">
<meta name="author" content="">
<%@ include file="title.jsp"%>
<%@ include file="import-header.jsp"%>
</head>

<body>
	<%@ include file="header.jsp"%>

	<div class="wrap-pad container">
		<div class="text-center">

			<h1>
				<i class="fa fa-book small-icons bk-color-blue"></i>Унесите Ваше податке
			</h1>
			<c:url var="loginUrl" value="/login" />
			<form:form method="POST" action="${loginUrl}">
				<c:if test="${param.error != null}">
					<div class="alert alert-danger">
						<p>Унето корисничко име и/или лозинка нису исправни!</p>
					</div>
				</c:if>
				<c:if test="${param.logout != null}">
					<div class="alert alert-success">
						<p>Успешно сте се одјавили!</p>
					</div>
				</c:if>
				<label for="username">Корисничко име</label>
				<input type="text" required="required" name="username" class="form-control" />
				<label for="password">Лозинка</label>
				<input type="password" required="required" name="password" class="form-control">
				<label for="submit"></label>
				<input type="hidden" name="${_csrf.parameterName}"
					value="${_csrf.token}" />

				<input type="submit" name="login" value="Пријавите се"
					class="form-control btn-info mt-2 text-center">
			</form:form>
		</div>
	</div>

	<%@ include file="footer.jsp"%>
</body>

</html>
