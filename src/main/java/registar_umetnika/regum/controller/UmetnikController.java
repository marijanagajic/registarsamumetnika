package registar_umetnika.regum.controller;

import java.sql.Date;
import java.util.List;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import registar_umetnika.regum.entity.Delatnost;
import registar_umetnika.regum.entity.Korisnik;
import registar_umetnika.regum.entity.Mesto;
import registar_umetnika.regum.entity.Referenca;
import registar_umetnika.regum.entity.Umetnik;
import registar_umetnika.regum.entity.Uplata;
import registar_umetnika.regum.service.interfaces.DelatnostService;
import registar_umetnika.regum.service.interfaces.KorisnikService;
import registar_umetnika.regum.service.interfaces.MestoService;
import registar_umetnika.regum.service.interfaces.ReferenceService;
import registar_umetnika.regum.service.interfaces.UmetnikService;
import registar_umetnika.regum.service.interfaces.UplateService;
import registar_umetnika.regum.util.DelatnostEditor;
import registar_umetnika.regum.util.KorisnikEditor;
import registar_umetnika.regum.util.MestoEditor;
import registar_umetnika.regum.util.UmetnikEditor;

@Controller
@RequestMapping("/administracija")
public class UmetnikController {

	@Autowired
	private UmetnikService umetnikService;
	@Autowired
	private UplateService uplateService;
	@Autowired
	private ReferenceService referenceService;
	@Autowired
	private KorisnikService korisnikService;
	@Autowired
	private MestoService mestoService;
	@Autowired
	private DelatnostService delatnostService;
	@Autowired
	private UmetnikEditor umetnikEditor;
	@Autowired
	private KorisnikEditor korisnikEditor;
	@Autowired
	private MestoEditor mestoEditor;
	@Autowired
	private DelatnostEditor delatnostEditor;

	@RequestMapping("/umetnici")
	public String prikaziUmetnike(Model theModel, HttpSession session) {
		List<Umetnik> umetnici = umetnikService.vratiAktivneUmetnike();

		theModel.addAttribute("umetnici", umetnici);
		String username = (String) session.getAttribute("user");
		Korisnik k = korisnikService.vratiKorisnika("username", username);
		theModel.addAttribute("korisnik", k);

		return "lista-umetnika";
	}
	
	@RequestMapping("/svi-umetnici")
	public String prikaziSveUmetnike(Model theModel) {
		List<Umetnik> umetnici = umetnikService.vratiUmetnike();
		theModel.addAttribute("umetnici", umetnici);

		return "lista-umetnika-svi";
	}
	
	@RequestMapping(value="/umetnici/deaktiviraj-umetnika", method=RequestMethod.POST)
	public String obrisiUmetnika(@RequestParam("umetnikID") String id, @RequestParam("datumUkidanjaStatusa") Date datum) {
		Umetnik u = umetnikService.vratiUmetnika("umetnikID", id);
		u.setStatus(0);
		u.setDatumukidanjastatusa(datum);
		umetnikService.sacuvajUmetnika(u);

		return "redirect:/administracija/umetnici";
	}
	
	@GetMapping("/umetnici/izmeni-podatke")
	public String izmeniUmetnika(@RequestParam("jmbg") String jmbg, Model model, HttpSession session) {
		Umetnik u = umetnikService.vratiUmetnika("jmbg", jmbg);
		model.addAttribute("umetnik", u);
		model.addAttribute("tipoviUmetnika", umetnikService.vratiTipoveUmetnika());
		model.addAttribute("udruzenjeId", u.getProglasenOdUdruzenja().getUdruzenjeId());
		String username = (String) session.getAttribute("user");
		Korisnik k = korisnikService.vratiKorisnika("username", username);
		model.addAttribute("korisnik", k);
		
		return "umetnik-forma";
	}
	
	@RequestMapping("/umetnici/{jmbg}")
	public String prikaziUmetnika(Model model, @PathVariable(name = "jmbg") String jmbg, HttpSession session) {
		Umetnik u = umetnikService.vratiUmetnika("jmbg", jmbg);
		String username = (String) session.getAttribute("user");
		Korisnik k = korisnikService.vratiKorisnika("username", username);
		model.addAttribute("korisnik", k);
		model.addAttribute("umetnik", u);

		return "profil-umetnika";
	}

	@RequestMapping("/umetnici/{jmbg}/uplate")
	public String prikaziUplateUmetnika(Model model, @PathVariable(name = "jmbg") String jmbg) {
		Umetnik u = umetnikService.vratiUmetnika("jmbg", jmbg);

		model.addAttribute("umetnik", u);

		List<Uplata> uplate = uplateService.vratiUplate(u.getUmetnikId());

		model.addAttribute("uplate", uplate);

		return "profil-umetnika-uplate";
	}
	
	@RequestMapping("/umetnici/{jmbg}/dodaj-referencu")
	public String dodajReferencuUmetniku(Model model, @PathVariable(name = "jmbg") String jmbg) {
		Referenca referenca = new Referenca();
		
		model.addAttribute("referenca", referenca);
		
		return "umetnik-referenca-forma";
	}
	
	@RequestMapping("/umetnici/{jmbg}/sacuvaj-novu-referencu")
	public String sacuvajReferencu(@PathVariable(name = "jmbg") String jmbg,
			@ModelAttribute("referenca") Referenca novaReferenca) {
		novaReferenca.setOdobren(0);
		novaReferenca.setUmetnik(umetnikService.vratiUmetnika("jmbg", jmbg));
		
		referenceService.sacuvajReferencu(novaReferenca);
		
		return "redirect:/administracija/umetnici/"+jmbg+"/reference";
	}

	@RequestMapping("/umetnici/{jmbg}/reference")
	public String prikaziReferenceUmetnika(Model model, @PathVariable(name = "jmbg") String jmbg) {
		Umetnik u = umetnikService.vratiUmetnika("jmbg", jmbg);

		model.addAttribute("umetnik", u);

		List<Referenca> reference = referenceService.vratiReference(u.getUmetnikId());

		model.addAttribute("reference", reference);

		return "profil-umetnika-reference";
	}

	@ModelAttribute("delatnosti")
	public List<Delatnost> delatnosti() {
		return delatnostService.vratiDelatnosti();
	}
	
	@ModelAttribute("mesta")
	public List<Mesto> mesta() {
		return mestoService.vratiMesta();
	}
	
	@InitBinder
	public void initBinder(WebDataBinder binder) {
		binder.registerCustomEditor(Umetnik.class, this.umetnikEditor);
		binder.registerCustomEditor(Korisnik.class, this.korisnikEditor);
		binder.registerCustomEditor(Mesto.class, this.mestoEditor);
		binder.registerCustomEditor(Delatnost.class, this.delatnostEditor);
	}
}
