package registar_umetnika.regum.service.interfaces;

import java.util.List;

import registar_umetnika.regum.entity.KulturnoPodrucje;
import registar_umetnika.regum.entity.Mesto;

public interface MestoService {

	List<Mesto> vratiMesta();
	Mesto vratiMesto(int id);

}
